import React, { Fragment, useState } from "react";
import Table from "../Table/Table.jsx";
import "./LoginForm-styles.css";
import { NavLink } from "react-router-dom";

function LoginForm() {
  let [alldata, setalldata] = useState([]);
  let [name, setName] = useState("");
  let [email, setMail] = useState("");
  let [dob, setdob] = useState("");
  let [pncode,setPncode] =useState()
  let [phoneNum, setNum] = useState("");
  let [city, setCity] = useState("");
  let [isValid,setIsValid]=useState(true)

  function handleClickOnSave() {

    if(isValid){
      let data = {
        name: "",
        email: "",
        dob: "",
        phoneNum: "",
        pincode: "",
        city: "",
      };
      data.name = name;
      data.email = email;
      data.dob = dob;
      data.phoneNum = phoneNum;
      data.city = city;
      data.pincode = pncode;
      setalldata([...alldata, data]);
      console.log(alldata);
  
      setName("");
      setMail("");
      setNum("");
      setCity("")
      setdob("")
      setPncode("")
  
    }
    else{
      alert('enter valid response')
    }

      }

  function Validate() {

    if(name.length<3 || pncode.length<8 || city.length < 3 ){
      setIsValid(false)
    }


    let validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (email.match(validRegex)) {
      setIsValid(true)
    } else {
      alert("Invalid email address!");
      document.form1.text1.focus();
      setIsValid(false)
    }


    let phone=phoneNum;
    if(phone.length==0 || phone.length!==10 || !phone.match(/^[0-9]{10}$/)){
      setIsValid(false)
    }
    

  }
  
  

  

  return (
    <Fragment>
      <div className="wrapper">
        <div className="container">
          <div className="inp-container">
            <p className="inp-info-text">Full Name</p>
            <input
              type="text"
              onChange={(e) => {
                setName(e.target.value);
              }}
              value={name}
              className="text-type-inp"
            />
          </div>

          <div className="inp-container">
            <p className="inp-info-text">Email Address</p>
            <input
              type="email"
              onChange={(e) => {
                setMail(e.target.value);
              }}
              value={email}
              className="text-type-inp"
            />
          </div>

          <div className="inp-container">
            <p className="inp-info-text">Phone Number</p>
            <input
              type="number"
              onChange={(e) => {
                setNum(e.target.value);
              }}
              value={phoneNum}
              className="text-type-inp"
            />
          </div>

          <div className="inp-container">
          <label className="inp-info-text" htmlFor="City">Date of birth</label>
          <input type="date" value={dob} onChange={(e)=>{setdob(e.target.value)}} className="text-type-inp" name="dob" id="" />
          </div>

          <div className="inp-container">
            <label className="inp-info-text" htmlFor="City">City</label>
            <input type="text" value={city} onChange={(e)=>{setCity(e.target.value)}} className="text-type-inp" name="dob" id="City" />            
          </div>

          <div className="inp-container">
            <label className="inp-info-text" htmlFor="pincode">Pin Code </label>
            <input type="number" value={pncode} onChange={(e)=>{setPncode(e.target.value)}} className="text-type-inp"  id="pincode" />            
          </div>

          <div className="inp-btn-container">
            <button className="btn" onClick={handleClickOnSave}>
              Save
            </button>
          </div>
        </div>

        <Table
          alldata={alldata}
          setalldata={setalldata}
          setCity={setCity}
          setdob={setdob}
          pncode={pncode}
          setName={setName}
          setMail={setMail}
          setNum={setNum}
        />
      </div>
    </Fragment>
  );
}

export default LoginForm;
