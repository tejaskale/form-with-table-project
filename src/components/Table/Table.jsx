import React, { Fragment } from 'react'
import "./table-styles.css"

function Table(props) {
    
    function handleDelete(index){
       const updatedData =  props.alldata.filter((item,deleteIndx)=>{
              return  index !== deleteIndx ;
        })
        props.setalldata(updatedData)
    }

    function handleEdit(index){
        const editableData =  props.alldata.filter((item,edtIndx)=>{
            return  index === edtIndx ;
      })

        const updatedData =  props.alldata.filter((item,deleteIndx)=>{
            return  index !== deleteIndx ;
        })
    props.setalldata(updatedData)


      let obj=editableData[0]
      props.setName(obj.name)
      props.setMail(obj.email)
      props.setdob(obj.dob)
      props.setNum(obj.phoneNum)
      props.setCity(obj.city)
      props.pncode(obj.pncode)
    }
   
   
  return (
    <Fragment>
           <div className='table-container' >
           <table>
            <thead>
              <tr>
              <th>id</th>
              <th> Name</th>
              <th>Email</th>
              <th>Phone No</th>
              <th>Date Of Birth</th>
              <th>City</th>
              <th>Pincode</th>
              <th>Action</th>
              </tr>
            </thead>
            <tbody>
            {
            props.alldata.map((current,index)=>{
                return(
                    <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{current.name}</td>
                    <td>{current.email}</td>
                    <td>{current.phoneNum}</td>
                    <td>{current.dob}</td>
                    <td>{current.city}</td>
                    <td>{current.pincode}</td>
                    <td>
                        <button className='edit-btn' onClick={()=>{handleEdit(index)}} >Edit</button>
                        <button className='dlt-btn' onClick={()=>{handleDelete(index) }} >Delete</button>
                    </td>
                </tr>
                )
            })    
            }
            </tbody>
        </table>
           </div>
    </Fragment>
  )
}

export default Table;