import logo from './logo.svg';
import './App.css';
import LoginForm from "./components/Login-Form/LoginForm"
import { BrowserRouter as Router , Routes , Route ,NavLink } from 'react-router-dom';
import Table from './components/Table/Table';

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path='/' element={< LoginForm />}/>
        <Route exact path='/table' element={< Table />}/>
      </Routes>
    </Router>

      )
}

export default App;
